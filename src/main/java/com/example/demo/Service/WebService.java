package com.example.demo.Service;
import com.example.demo.Utils.Constants;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class WebService {
    public static String fetchJson(String ticker,String startDate,String endDate){
        String jsonString = "";
        try {
            URL url = new URL("https://www.quandl.com/api/v3/datasets/WIKI/"+ticker+"/data.json?&start_date="+startDate+"&end_date="+endDate+"&api_key="+Constants.api_key);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(RequestMethod.GET.toString());
            conn.setRequestProperty("Accept","application/json");
            if (conn.getResponseCode() != 200){
                return "";
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            jsonString = org.apache.commons.io.IOUtils.toString(br);

            conn.disconnect();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return jsonString;
    }
}

