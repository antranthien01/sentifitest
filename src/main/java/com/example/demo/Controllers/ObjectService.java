package com.example.demo.Controllers;

import com.example.demo.DTO.DateClose;
import com.example.demo.DTO.Price;
import com.example.demo.Models.DataObject;
import com.example.demo.Service.WebService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ObjectService {

    Price getClosePrices(String tickerName,String startDate,String endDate){
        DataObject object = new DataObject();
        String jsonString = WebService.fetchJson(tickerName,startDate,endDate);

        // Handle if server return failed
        if (jsonString == ""){
            return null;
           // return " \"code\": 404,\n" +
             //       " \"message\": \"Wrong ticker symbol\"";
        }
        
        ObjectMapper mapper = new ObjectMapper();
        try{
            object = mapper.readValue(jsonString,DataObject.class);

        }catch (Exception e){
            e.printStackTrace();
        }

        System.out.print(jsonString);
        List<List<String>> listData = object.getDatasetData().getData();
        Price price = new Price();
        ArrayList<DateClose> dateCloses = new ArrayList<>();

        for (List<String> list:listData) {
            DateClose date = new DateClose();
            date.setDate(list.get(0));          // get date
            date.setClosePrice(list.get(4));    //get close price
            dateCloses.add(date);
        }
        price.setTicker(tickerName);
        price.setDateCloses(dateCloses);

        return price;


    }
    String getClosePricesToJSonString(String tickerName,String startDate,String endDate){
        String outputString = "";
        ObjectMapper mapper = new ObjectMapper();
        Price price = getClosePrices(tickerName,startDate,endDate);
        try {
            outputString = mapper.writeValueAsString(price);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return outputString;
    }
}
