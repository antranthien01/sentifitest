package com.example.demo.Controllers;

import com.example.demo.DTO.DateClose;
import com.example.demo.DTO.Price;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.time.format.DateTimeFormatter;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@RequestMapping("/api/v2")
public class PricesController {
    @Autowired
    ObjectService objectService;

    @RequestMapping(value = "/{ticker_symbol}/closePrice",method = RequestMethod.GET)
    public String getPrice(@PathVariable(value = "ticker_symbol") String ticker,
                           @RequestParam(value = "startDate") String startDate,
                           @RequestParam(value = "endDate") String endDate){

        String checkDateStatus = checkDateValid(startDate,endDate);
        String output = objectService.getClosePricesToJSonString(ticker,startDate,endDate);

        return checkDateStatus == ""? output: checkDateStatus;

    }
    @RequestMapping(value = "/hello",method = RequestMethod.GET)
    public String getHello(){
        return "Hello";
    }

    @RequestMapping(value = "/{ticker_symbol}/200dma",method = RequestMethod.GET)
     public String getLast200dma(@PathVariable("ticker_symbol") String ticker,
                                 @RequestParam("startDate") String startDate){
        System.out.print("12312434");
        String startDateStatus = checkStartDateFor200dmaValid(startDate);
        if (startDateStatus!=""){
            return startDateStatus;
        }else{
            return get200dmaJsonString(ticker,startDate);
        }
    }

    String get200dmaJsonString(String ticker,String startDate){
        LocalDate endDate = LocalDate.parse(startDate).plusDays(199);
        String endDateString = endDate.format(DateTimeFormatter.ISO_LOCAL_DATE);
        Price priceOjb = objectService.getClosePrices(ticker,startDate,endDateString);
        long dateNum = 0;
        Double sumPrice = 0.0;
        Double averagePrice;

        for (DateClose dateClose:priceOjb.getDateCloses()) {
            dateNum += 1;
            sumPrice += Double.parseDouble(dateClose.getClosePrice());
        }

        if (dateNum != 0){
            averagePrice = sumPrice/dateNum;
        }
        else{
            averagePrice = 0.0;
        }
        return "{\n" +
                "    \"200dma\": {\n" +
                "      \"Ticker\": \""+ticker+"\",\n" +
                "      \"Avg\": \""+averagePrice.toString()+"\"\n" +
                "    }\n" +
                "}\n";

    }
    String checkStartDateFor200dmaValid(String startDate){
        String status="";
        Date currentDate = new Date();
        Date start_Date;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");

        try{
            start_Date = simpleDateFormat.parse(startDate);
        }catch (Exception e){
            e.printStackTrace();
            return " \"message\": \"Wrong start day format\"";
        }
        long daysDiff = ChronoUnit.DAYS.between(start_Date.toInstant(),currentDate.toInstant());
        if (daysDiff < 200){
            String validDateString = getValidDayFor200dma();
            return " \"message\": \"Days from given start day to now is not enough 200. Valid date is "+validDateString+"\"";
        }
        return status;
    }
    String getValidDayFor200dma(){
        LocalDate validDate = LocalDate.now().minusDays(200);
        String formattedString = validDate.format(DateTimeFormatter.ISO_LOCAL_DATE);

        return  formattedString;
    }

    String checkDateValid(String startDate, String endDate){
        String status ="";
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-mm-dd");
        Date start_Date;
        Date end_Date;

        try{
            start_Date = dateFormatter.parse(startDate);
        }catch (Exception e){
            e.printStackTrace();
            return " \"message\": \"Wrong start day format\"";
        }

        try {
            end_Date = dateFormatter.parse(endDate);
        }catch (Exception e){
            e.printStackTrace();
            return " \"message\": \"Wrong end day format\"";
        }

        if (end_Date.before(start_Date)){
            return " \"message\": \"End day must be  more than Start day\"";
        }
        return  status;
    }

}
