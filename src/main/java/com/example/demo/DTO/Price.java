package com.example.demo.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class Price {
    @JsonProperty("ticker")
    private String ticker;

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public ArrayList<DateClose> getDateCloses() {
        return dateCloses;
    }

    public void setDateCloses(ArrayList<DateClose> dateCloses) {
        this.dateCloses = dateCloses;
    }
    @JsonProperty("DateCloses")
    private ArrayList<DateClose> dateCloses;
}
