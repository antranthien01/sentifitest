package com.example.demo.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DateClose {
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getClosePrice() {
        return closePrice;
    }

    public void setClosePrice(String closePrice) {
        this.closePrice = closePrice;
    }
    @JsonProperty("price")
    private String closePrice;

    @JsonProperty("date")
    private String date;



}
